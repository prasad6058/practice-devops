import express from 'express';
import os from 'os'

const app = express();
const port = 3000;

app.get('/', (req, res) => {
  res.send(` Hello, World! V2 from ${os.hostname}`);
});

app.listen(port, () => {
  console.log(`Server is running on port ${port}`);

});
